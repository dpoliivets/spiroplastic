import React from "react"
import { StaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"


const Sadat = () => (
  <StaticQuery
    query={graphql`
      query {
        placeholderImage: file(relativePath: { eq: "sadat.jpg" }) {
          childImageSharp {
            fluid(maxWidth: 300) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    `}
    render={data => <Img fluid={data.placeholderImage.childImageSharp.fluid} style={{ borderTopLeftRadius: 4, borderTopRightRadius: 6 }} />}
  />
)
export default Sadat
