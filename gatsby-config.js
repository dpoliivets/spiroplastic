module.exports = {
  siteMetadata: {
    title: `Spiroplastic`,
    description: `Leading acrylic sheet manufacturer in Egypt.`,
    author: `Dmitry Poliyivets (dpoliivets.com)`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/assets/images/`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Spiroplastic`,
        short_name: `Spiroplastic`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#ffffff`,
        display: `minimal-ui`,
        icon: `src/assets/icons/favicon.svg`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: "gatsby-plugin-react-svg",
      options: {
        rule: {
          include: /icons/
        }
      }
    },
    {
      resolve: `gatsby-plugin-prefetch-google-fonts`,
      options: {
        fonts: [
          {
            family: `Archivo`,
            subsets: [`latin`],
            variants: [`400`, `800`]
          },
          {
            family: `Open Sans`,
            subsets: [`latin`],
            variants: [`300`, `600`]
    },
        ],
        fontDisplay: 'swap'
      },
    },
    `gatsby-plugin-offline`,
  ],
}
